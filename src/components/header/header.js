import React, { Component } from 'react';
import './header.css';

export default class Header extends Component {
    render () {
        const {onReq} = this.props;
        return <header className="header d-flex align-items-center justify-content-between col-12 bg-light"><div className="col-5"><h3>Future App</h3></div><div className="col-7 d-flex justify-content-end"><button className="btn btn-large btn-custom" onClick={()=>{onReq(32)}}>32 строки</button><button className="btn btn-large btn-custom" onClick={()=>{onReq(1000)}}>1000 строк</button></div></header>
    }
}

