import React, { Component } from 'react';

//components
import DataTable from '../data-table/';
import Header from '../header/';
import SearchBar from '../search-bar/';
import ErrorMessage from '../error-message/'

//services
import Filltext from '../../services/filltext';

import './app.css';

export default class App extends Component {
    filltext = new Filltext();
    state = {
        data: null,
        loading: false,
        searchQuery: '',
        hasError: false
    };
    searchQuery = (query) => {
        this.setState({
            searchQuery: query.toString().toLowerCase()
        })
    }
    loadData = (rows) => {
        this.setState({
            loading: true,
            searchQuery: ''
        });
        this.filltext.getData(rows)
                    .then(this.onDataLoaded)
                    .catch(this.onError);
        }
    
    onDataLoaded = (data) =>{
        this.setState({
            data,
            loading: false
        })
    }
    onError() {
        this.setState({
            hasError: true
        })
        console.log('error');
    }
    searchTask(items, query) {
        if(query.length === 0) {
            return items;
        }
        return items.filter((item)=>{
            return item.firstName.toLowerCase().indexOf(query) > -1 || item.lastName.toLowerCase().indexOf(query) > -1 || item.email.toLowerCase().indexOf(query) > -1 ||  item.phone.toLowerCase().indexOf(query) > -1 || item.id.toString().indexOf(query) > -1;
        });
    }
    componentDidCatch() {
        this.onError();
    }
    render  () {
        const { data, loading, searchQuery, hasError } = this.state;
        const visibleItems = data ? this.searchTask(data, searchQuery): null;
        if(hasError) {
            return <ErrorMessage />
        }
            return (
                <React.Fragment>
                    <Header onReq = { this.loadData }/>
                    <SearchBar onSearch = { this.searchQuery }/>
                    <DataTable 
                        data = { visibleItems }
                        loading = { loading }/>
                </React.Fragment>
                )
    }
}