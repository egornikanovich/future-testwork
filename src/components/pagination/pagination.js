import React, { Component } from 'react';
import './pagination.css';

export default class Pagination extends Component {
    render () {
        const {rows, currentPage, maximumItemsPerPage, onSetPage} = this.props;
        const lastPage = Math.floor(rows/maximumItemsPerPage);
        const first = (currentPage-3)>=1 ? <li className="page-item"><span className="page-link" onClick={()=>onSetPage(1)}>1</span></li> : null;
        const minusOne = (currentPage-1)>=1 ? <li className="page-item"><span className="page-link" onClick={()=>onSetPage(currentPage-1)}>{currentPage-1}</span></li> : null;
        const minusTwo = (currentPage-2)>=1 ? <li className="page-item"><span className="page-link" onClick={()=>onSetPage(currentPage-2)}>{currentPage-2}</span></li> : null;
        const plusOne = (currentPage+1)<=lastPage ? <li className="page-item"><span className="page-link" onClick={()=>onSetPage(currentPage+1)}>{currentPage+1}</span></li> : null;
        const plusTwo = (currentPage+2)<=lastPage ? <li className="page-item"><span className="page-link" onClick={()=>onSetPage(currentPage+2)}>{currentPage+2}</span></li> : null;
        const last = (currentPage+3)<=lastPage ? <li className="page-item"><span className="page-link" onClick={()=>onSetPage(lastPage)}>{lastPage}</span></li> : null;
        return (
            <nav>
                <ul className="pagination">
                {first}
                {minusTwo}
                {minusOne}
                <li className="page-item active"><span className="page-link active" onClick={()=>false}>{currentPage}</span></li>
                {plusOne}
                {plusTwo}
                {last}
                </ul>
            </nav>
        )
    }
}

