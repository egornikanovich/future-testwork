import React, { Component } from 'react';
import './data-table.css';
import Spinner from '../spinner/';
import SelectedItem from '../selected-item/';
import Pagination from '../pagination/';
import ErrorMessage from '../error-message/';

export default class DataTable extends Component {
    state =  {
        filter: null,
        ascending: true,
        hasError: false,
        selectedId: null,
        currentPage: 1,
        maximumItemsPerPage: 50,
        selected: false
    }
    componentDidUpdate(prevProps) {
        if(prevProps.data !== this.props.data) {
            this.setState({
                currentPage: 1, 
                filter: null,
                ascending: true,
                selectedId: null,
                selected: false
            });
        }
    }
    setFilter (par) {
        if(this.state.filter !== par) {
            this.setState({
                filter: par,
                ascending: true,
                selectedId: null
            })
        } else {
            const { ascending } = this.state;
            this.setState({
                filter: par,
                ascending: !ascending,
                selectedId: null
            })
        } 
    }
    selectItem = (id) => {
        this.setState({
            selectedId: id,
            selected: true
        })
    }
    setPage = (numb) => {
        this.setState({
            currentPage: numb,
            selected: false
        })
    }
    componentDidCatch () {
        this.onError();
    }
    onError() {
        this.setState({
            hasError: true
        })
        console.log('error');
    }
    renderItems(array) {
        let sorted=[];
        switch(this.state.filter) {
            case('id'): {
                sorted = this.state.ascending ? array.sort((obj1, obj2)=>obj1.id-obj2.id) : array.sort((obj1, obj2)=>obj2.id-obj1.id);
                break;
            }
            case('firstName'): {
                sorted = this.state.ascending ? array.sort((obj1, obj2)=>obj1.firstName.toLowerCase()>obj2.firstName.toLowerCase()) : array.sort((obj1, obj2)=>obj1.firstName.toLowerCase()<obj2.firstName.toLowerCase());
                break;
            }
            case('lastName'): {
                sorted = this.state.ascending ? array.sort((obj1, obj2)=>obj1.lastName.toLowerCase()>obj2.lastName.toLowerCase()) : array.sort((obj1, obj2)=>obj1.lastName.toLowerCase()<obj2.lastName.toLowerCase());
                break;
            }
            case('email'): {
                sorted = this.state.ascending ? array.sort((obj1, obj2)=>obj1.email.toLowerCase()>obj2.email.toLowerCase()):array.sort((obj1, obj2)=>obj1.email.toLowerCase()<obj2.email.toLowerCase());
                break;
            }
            case('phone'): {
                sorted = this.state.ascending ? array.sort((obj1, obj2)=>obj1.phone.toLowerCase()>obj2.phone.toLowerCase()) : array.sort((obj1, obj2)=> obj1.phone.toLowerCase()<obj2.phone.toLowerCase());
                break;
            }
            default: {
                sorted = array;
                break;
            }
        }
        const firstItemIndex = (this.state.currentPage-1)*this.state.maximumItemsPerPage;
        const lastItemIndex = firstItemIndex - 1 + this.state.maximumItemsPerPage;
        return sorted.map((element, index)=>{
            const uniqueId = element.id+element.lastName+element.firstName;
            if(index>=firstItemIndex && index<=lastItemIndex) {
                return ( 
                    <tr key={ uniqueId } onClick = {() => this.selectItem(index)} className="item-row">
                        <td className="item-row-td">{index+1}</td>
                        <td className="item-row-td">{element.id}</td>
                        <td className="item-row-td">{element.firstName}</td>
                        <td className="item-row-td">{element.lastName}</td>
                        <td className="item-row-td">{element.email}</td>
                        <td className="item-row-td">{element.phone}</td>
                    </tr>);
            } else {
                return null;
            }
        });
    }
    render () {
        const { filter, ascending, currentPage, 
            maximumItemsPerPage, selectedId, selected, hasError } = this.state;
        const error = hasError ? <ErrorMessage /> : null
        const { data, loading } = this.props;
        const items = data && !loading && !hasError ? this.renderItems(data) : null;
        const spinner = loading ? <Spinner /> : null;
        const selectedItem = selected && !loading ? <SelectedItem person={data[selectedId]}/> : null;
        const pagination = data && !loading && !hasError ? <Pagination rows = { data.length } currentPage = { currentPage } maximumItemsPerPage = { maximumItemsPerPage } onSetPage = { this.setPage }/> : null; 
        const idFilter = filter ==='id' && ascending ? '▲': '▼';
        const firstNameFilter = filter==='firstName' && ascending ? '▲': '▼';
        const lastNameFilter = filter==='lastName' && ascending ? '▲': '▼';
        const emailFilter = filter==='email' && ascending ? '▲': '▼';
        const phoneFilter = filter==='phone' && ascending ? '▲': '▼';
        return (
                <React.Fragment>
                    <div className="col-12 bg-light">
                        <table className="table data-table">
                            <thead>
                                <tr>
                                    <th scope="col" className="heading">#</th>
                                    <th scope="col" className="heading" onClick={()=>this.setFilter('id')}>id { idFilter }</th>
                                    <th scope="col" className="heading" onClick={()=>this.setFilter('firstName')}>firstName { firstNameFilter }</th>
                                    <th scope="col" className="heading" onClick={()=>this.setFilter('lastName')}>lastName { lastNameFilter }</th>
                                    <th scope="col" className="heading" onClick={()=>this.setFilter('email')}>email { emailFilter }</th>
                                    <th scope="col" className="heading" onClick={()=>this.setFilter('phone')}>phone { phoneFilter }</th>
                                </tr>
                            </thead>
                            <tbody>
                                { items }
                            </tbody>
                        </table>
                        <div className="justify-content-center d-flex">
                        { error }
                        { spinner }
                        </div>
                        <div className="col-12 d-flex justify-content-center">
                            { pagination }
                        </div>
                    </div>
                    <div className="col-12 col-md-8 col-lg-6">
                        { selectedItem }
                    </div>
                </React.Fragment>
        )
    }
}

