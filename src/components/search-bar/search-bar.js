import React, { Component } from 'react';
import './search-bar.css';

export default class SearchBar extends Component {
    onSearch = () => {
        const { onSearch } = this.props;
        const value = document.getElementsByClassName('form-control')[0].value;
        onSearch(value);
    }
    render () {
        return  (<div className="form-group col-12 bg-light search-bar d-flex justify-content-between align-items-center">
            <div className="col-9">
                <input type="text" className="form-control" placeholder="Поиск..."/>
            </div>
            <div className="col-3">
                <button className="btn btn-large btn-custom btn-find" onClick = { this.onSearch }>Найти!</button>
            </div>
        </div>)
    }
}

