import React from 'react';

import './error-message.css';
import icon from './error.png';

const ErrorMessage = () => {
    return (
        <div className="d-flex flex-column align-items-center justify-content-center bg-light error">
            <img className="error-icon" src={icon} alt="error img"/>
            <h3 className="error-header">OOPS!</h3>
            <span className="error-text">Something wrong happened!</span>
        </div>
    )

}
export default ErrorMessage;