import React, { Component } from 'react';
import ErrorMessage from '../error-message/'

import './selected-item.css';

export default class SelectedItem extends Component {
    state = {
        hasError: false
    }
    componentDidCatch () {
        this.onError();
    }
    onError() {
        this.setState({
            hasError: true
        })
        console.log('error');
    }
    render () {
        if(this.state.hasError) {
            return (<div className="bg-light selected-item d-flex justify-content-center align-items-center">
                    <ErrorMessage />
                </div>)
        }
        const {person} = this.props;
        if (person) {
        return (<div className="bg-light selected-item">
            <p>Выбран пользователь <b>{person.firstName} {person.lastName}</b></p>
            <p>Описание:</p>
            <textarea value={person.description} rows="4" cols="50" onChange={()=>false}></textarea>
            <p>Адрес проживания: <b>{person.address.streetAddress}</b></p>
            <p>Город: <b>{person.address.city}</b></p>
            <p>Провинция/штат: <b>{person.address.state}</b></p>
            <p>Индекс: <b>{person.address.zip}</b></p>
        </div>)
        } else {
            return null;
        }
    }
}

