export default class Filltext {

    _apiBase = 'http://www.filltext.com/';

    async getData(rows) {
        const data = await fetch(`${ this._apiBase }?rows=${ rows }&id={number|1000}&firstName={firstName}&lastName={lastName}&email={email}&phone={phone|(xxx)xxx-xx-xx}&address={addressObject}&description={lorem|32}`);
        if(!data.ok) {
            throw new Error(`Ресурс не загружен, получена ошибка: ${ data.status }`);
        }
        return await data.json();
    }
}
